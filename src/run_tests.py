import unittest

loader = unittest.loader.TestLoader()
suite = loader.discover('tests')
unittest.TextTestRunner(verbosity=2).run(suite)
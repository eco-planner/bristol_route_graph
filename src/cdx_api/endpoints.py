from flask import render_template
from cdx_api import app
from models import predict_model, train_model

@app.route('/')
def index():
    #for future use
    return 'Set up complete'

@app.route('/train')
def train():
    #train model
    return train_model.train()

@app.route('/predict')
def predict():
    return predict_model.predict()

@app.route('/status')
def status():
    return 'status'

import os
from flask import Flask

app = Flask(__name__)
# app.config.from_object('cdx_api.default_settings')
# app.config.from_envvar('CDX_API_SETTINGS')

if not app.debug:
    import logging
    from logging.handlers import TimedRotatingFileHandler
    # https://docs.python.org/3.6/library/logging.handlers.html#timedrotatingfilehandler
    # file_handler = TimedRotatingFileHandler(os.path.join(app.config['LOG_DIR'], 'cdx_api.log'), 'midnight')
    # file_handler.setLevel(logging.WARNING)
    # file_handler.setFormatter(logging.Formatter('<%(asctime)s> <%(levelname)s> %(message)s'))
    # app.logger.addHandler(file_handler)

import cdx_api.endpoints

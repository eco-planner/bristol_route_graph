#!/usr/bin/env bash

# Modify these two as needed:
COMPOSE_FILE_LOC="docker-compose.test.yml"
TEST_CONTAINER_NAME="app_test"

COMPOSE_PROJECT_NAME_ORIGINAL="jenkinsbuild_${BUILD_TAG}"

# Project name is sanitized by Compose, so we need to do the same thing.
# See https://github.com/docker/compose/issues/2119.
COMPOSE_PROJECT_NAME=$(echo $COMPOSE_PROJECT_NAME_ORIGINAL | awk '{print tolower($0)}' | sed 's/[^a-z0-9]*//g')
TEST_CONTAINER_REF="${COMPOSE_PROJECT_NAME}_${TEST_CONTAINER_NAME}_1"

# Record installed version of Docker and Compose with each build
echo "Docker environment:"
docker --version
docker-compose --version

function cleanup {
    # Shutting down all containers associated with this project
    docker-compose -f $COMPOSE_FILE_LOC \
                   -p $COMPOSE_PROJECT_NAME \
                   down --remove-orphans
    docker ps -a --no-trunc  | grep $COMPOSE_PROJECT_NAME \
        | awk '{print $1}' | xargs --no-run-if-empty docker stop
    docker ps -a --no-trunc  | grep $COMPOSE_PROJECT_NAME \
        | awk '{print $1}' | xargs --no-run-if-empty docker rm
}

function run_tests {
    # Create containers
    docker-compose -f $COMPOSE_FILE_LOC \
                   -p $COMPOSE_PROJECT_NAME \
                   up -d --build --force-recreate
    # List images and containers related to this build
    rm -f image_id.txt
    CREATED_IMAGE_ID=$(docker images | grep $COMPOSE_PROJECT_NAME | awk '{print $3}')
    echo $CREATED_IMAGE_ID
    echo $CREATED_IMAGE_ID > 'image_id.txt'
    docker images | grep $COMPOSE_PROJECT_NAME | awk '{print $0}'
    docker ps -a | grep $COMPOSE_PROJECT_NAME | awk '{print $0}'
    docker exec ${TEST_CONTAINER_REF} sh -c 'nosetests --with-xunit  --verbosity=2 --with-coverage --cover-package=.'
    # Follow the container with tests...
    docker logs $TEST_CONTAINER_REF
}

function extract_results {
    docker cp ${TEST_CONTAINER_REF}:/app/nosetests.xml .

}

function on_err {
docker logs $TEST_CONTAINER_REF
extract_results
exit
}
set -o errtrace
cleanup            # Initial cleanup
trap on_err ERR
trap cleanup EXIT  # Cleanup after tests finish running

run_tests
extract_results



